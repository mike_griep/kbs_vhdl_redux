#include <stdio.h>
#include <stdlib.h>
#include "includes.h"
#include "Generator.h"
#include <stdint.h>

//	because for(EVER) looks prettier than while(1)
#define EVER ;;

/* Definition of Task Stacks */
#define   TASK_STACKSIZE       1024
OS_STK    RK_stk[TASK_STACKSIZE];
OS_STK	  G_stk[TASK_STACKSIZE];
OS_STK	  LV_stk[TASK_STACKSIZE];

/* Definition of Task Priorities */

#define TASKRK_PRIORITY      9
#define TASKG_PRIORITY	 	10
#define TASKLV_PRIORITY		11

/* Definition of colours */
#define VGAColour_Red		0xF800
#define VGAColour_Maroon	0x7800
#define	VGAColour_Green		0x07E0
#define VGAColour_Blue		0x001F
#define VGAColour_Ltblue	0x7800 | 0x03E0 | 0x001F
#define VGAColour_Orange 	0xFBE0
#define VGAColour_White 0xFFFF
#define VGAColour_Black 0x0000

void clear_screen();
uint16_t get_Pixel(uint16_t x, uint16_t y);
void write_pixel(uint16_t x, uint16_t y, uint16_t colour);
void write_char(int x, int y, char c);
void write_rect(int x1, int x2, int y1, int y2, short colour);
void write_level_line(int x1, int x2, int xgap, int y, short colour);
void write_string(int x, int y, char *s);
void PlayerF(char);
void Menu();
void GameStart();

struct Player{
	int x1,x2,y1,y2;
} _playertoon;

int s, menuon, u, go, gamespd, gamectr, movemod;
volatile short *vga_addr=(volatile short*)(0x08000000);

/* helper functies */

/* Turns the screen black using write_pixel and write_char
 * Author: Max Steendijk
 */
void clear_screen()
{
	int x, y;
	for (x = 0; x < 320; x++)
	{
		for (y = 0; y < 240; y++)
		{
			write_pixel(x, y, 0);
		}
	}
	for (x = 0; x < 80; x++)
	{
		for (y = 0; y < 60; y++)
			write_char(x, y, '\0');
	}
}

// get the pixel at that pos from the memory.
uint16_t get_Pixel(uint16_t x, uint16_t y){
	int offset = (y << 9) | x;
	return *(vga_addr + offset);
}

/*	set a single pixel at x,y to colour colour
 * Sourced from example documentation, modified by Mike Griep*/
void write_pixel(uint16_t x, uint16_t y, uint16_t colour)
{
	//volatile short *vga_addr=(volatile short*)(0x08000000 + (y<<10) + (x<<1));
	//*vga_addr = colour;
	int offset = (y << 9) | (x);
	*(vga_addr + offset) = colour;
}

/* write a single character to the character buffer at x,y
 * Sourced from example documentation*/
void write_char(int x, int y, char c)
{
	volatile char * character_buffer = (char*) (0x09000000 + (y<<7) + x);
	*character_buffer = c;
}

/* Draw a vertical line segment from (x, y1) to (x, y2) in the colour colour
 * Author: Max Steendijk*/
void write_line_vert(int x, int y1, int y2, short colour)
{
	int yc;
	if (x >= 0 && x <= 319)
	for (yc=y1; yc <= y2; yc++)
		write_pixel(x, yc, colour);
}

/* Draw a horizontal line segment from (x1, y) to (x2, y) in the colour colour
 * Author: Max Steendijk*/
void write_line_hori(int x1, int x2, int y, short colour)
{
	int xc;
	if (y >= 0 && y <= 239)
	for (xc=x1; xc<=x2; xc++)
		write_pixel(xc, y, colour);
}

/* Draw a rectangle of the specified colour with the corners (x1,y1), (x1,y2), (x2,y1) and (x2,y2)
 * Author: Max Steendijk*/
void write_rect(int x1, int x2, int y1, int y2, short colour)
{
	int xc, yc;
	for (xc = x1; xc <= x2; xc++)
	{
		write_pixel(xc, y1, colour);
		write_pixel(xc, y2, colour);
	}
	for (yc = y1; yc <= y2; yc++)
	{
		write_pixel(x1, yc, colour);
		write_pixel(x2, yc, colour);
	}

}

/* Draw two horizontal lines between (x1, y) and (x2, y) with a gap between (xgap,y) and (xgap+30,y)
 * Author: Max Steendijk*/
void write_level_line(int x1, int x2, int xgap, int y, short colour)
{
	if(xgap > x1)
		write_line_hori(x1,xgap,y,colour);
	if(xgap+30 < x2)
		write_line_hori(xgap+30,x2,y,colour);
}

/* Write a series of characters starting at (x,y)
 * Author: Max Steendijk*/
void write_string(int x, int y, char *s)
{
	int xc, i = 0;
	xc = x;
	while(s[i] != '\0')
	{
		write_char(xc, y, s[i]);
		//	TODO: make this more efficient
		xc++;
		i++;
	}
}

/* Draws the menu
 * Author: Max Steendijk
 * */
void Menu(){

	printf("menu init'd\n");
	int txtY = 37;
		/* draw lines at
		 * (80,130) horizontally until (240,130)
		 * (80,130) vertically until (80,170)
		 * (80,170) horizontally until (240,170)
		 * (240,130) vertically until (240,170)
		 */

	clear_screen();
	/* Logo */
	//C
	write_line_hori(32,61,25,VGAColour_Orange);
	write_line_hori(32,61,94,VGAColour_Orange);
	write_line_vert(32,25,94,VGAColour_Orange);
	//L
	write_line_vert(66,25,94,VGAColour_Orange);
	write_line_hori(66,94,94,VGAColour_Orange);
	//I
	write_line_vert(103,25,94,VGAColour_Orange);
	//M
	write_line_vert(116,25,94,VGAColour_Orange);
	write_line_hori(116,166,38,VGAColour_Orange);
	write_line_vert(166,25,94,VGAColour_Orange);
	write_line_vert(141,38,50,VGAColour_Orange);
	//B
	write_line_vert(179,25,94,VGAColour_Orange);
	write_line_hori(175,208,25,VGAColour_Orange);
	write_line_vert(208,25,94,VGAColour_Orange);
	write_line_hori(175,208,94,VGAColour_Orange);
	write_line_hori(179,208,55,VGAColour_Orange);
	//E
	write_line_vert(217,25,94,VGAColour_Orange);
	write_line_hori(213,246,25,VGAColour_Orange);
	write_line_hori(213,246,94,VGAColour_Orange);
	write_line_hori(217,236,55,VGAColour_Orange);
	//R
	write_line_vert(255,25,94,VGAColour_Orange);
	write_line_vert(274,55,94,VGAColour_Orange);
	write_line_hori(255,284,25,VGAColour_Orange);
	write_line_hori(255,284,55,VGAColour_Orange);
	write_line_vert(284,25,55,VGAColour_Orange);
	/* end of logo */

	write_rect(80, 240, 130, 170, VGAColour_Red);	//	Draws the box for the message to be written in. Looks a little nicer.
	write_string(30, txtY, "Press ENTER to begin");

		/* Voor het geval dat we tijd over hebben, extra ruimte voor knoppen voor Highscores e.d.*/
		//	write_rect(80, 240, 130, 150, VGAColour_Red);
		//	write_rect(80, 240, 160, 180, VGAColour_Red);

}

/* Draws the level's map and the first few lines of the map */
void GameStart(){
	//	TODO: Become the level gen
	int i, err;

	write_string(65,4,"Score:");

	//	draw the lines that make up the field
	//	Level boundaries
	write_line_vert(9, 0, 239, VGAColour_Maroon);
	write_line_vert(250, 0, 239, VGAColour_Maroon);

	/* stops the program if the levels fail to initialise*/
	err = levelInit();
	if(err != 0)
	{
		printf("Level init failed");
		return;
	}

	/* draws all the lines for the first time */
	for(i = 0; i < MAX_STORED_LINES; i++)
	{
		if(Lines[i].gap == 0 && Lines[i].y >= 0)	//	as stated in Generator.h, lvlLine.gap == 0 -> no gap.
			write_line_hori(10, 249, Lines[i].y, VGAColour_Red);	//	The base line is drawn in a different colour
		else if(Lines[i].y >= 0)					//	just making sure that we're not drawing anything outside the VGA buffer
			write_level_line(10, 249, Lines[i].gap, Lines[i].y, VGAColour_Ltblue);
	}
}

/* Tasks */

/*	TaskLevel
 * 	OS task
 *
 * 	Initiates the map and moves the map's lines and the player.
 * 	Also manages difficulty and score
 *
 * 	Author: Max Steendijk
 */
void TaskLevel(void* pdata)
{
	/*	will:
	 * 		1 Generate the level
	 * 		2 Pause for two seconds
	 * 		3 Use LevelStep to step the level down
	 * 		4 Redraw the lines whenever the LevelStep is done
	 * 		5 GOTO 3 until player has failed
	 */
	int i;
	int s = 0;
	int score;
	int mod;
	char* scoreStr;
	printf("TaskLevel operational\n");
	for(EVER)
	{
		while(!menuon){
			s = 0;
			GameStart();
			gamespd = 600;
			gamectr = 0;
			score = 0;
			mod = 1;
			OSTimeDlyHMSM(0,0,2,0);
			for(EVER){
				if (menuon)
					//	menuon is rechecked to make sure that the game doesn't continue drawing when the player exits the game
					break;
				if(!go)
				{
					levelStep();	//	moving lines

					write_rect(_playertoon.x1, _playertoon.x2, _playertoon.y1, _playertoon.y2, VGAColour_Black);
					for(i = 0; i < MAX_STORED_LINES; i++)
					{
						if(Lines[i].gap == 0 && Lines[i].y >= 0)
						{
							write_line_hori(10, 249, Lines[i].y, VGAColour_Red);
							write_line_hori(10, 249, Lines[i].y - LVL_STEP_DST, 0);
						}
						else if(Lines[i].y >= 0)
						{
							write_level_line(10, 249, Lines[i].gap, Lines[i].y, VGAColour_Ltblue);
							write_level_line(10, 249, Lines[i].gap, Lines[i].y - LVL_STEP_DST, 0);
						}
					}

					_playertoon.y1 += LVL_STEP_DST;
					_playertoon.y2 += LVL_STEP_DST;
					write_rect(_playertoon.x1, _playertoon.x2, _playertoon.y1, _playertoon.y2, VGAColour_White);

					//	making sure the bottom line is clear
					write_line_hori (10, 249, 238, 0);

					gamectr++;							//	increment counter for difficulty increase
					score += mod;						//	increment score
					sprintf(scoreStr, "%6d\0", score);	//	write score to a string
					write_string(71,4,scoreStr);		//	write the string to the screen
					if (gamectr > 49)
					{
						//	improve score rate, and accellerate game
						mod++;
						gamectr = 0;
						if (gamespd >= 200)
							gamespd -= 50;
					}
				}
				OSTimeDlyHMSM(0,0,0, gamespd);
			}
		}
		if (!s)
		{
			s++;

		}
		//	just to make sure TaskLevel doesn't hog the operating system
		OSTimeDlyHMSM(0,0,0,250);
	}


}

/*
 * This task does some of the gamelogic and most of the playerlogic.
 * Author: Mike Griep
 */
void TaskGame(void* pdata){

	printf("TaskGame exists\n");
	//Create the character and load the menus.
	_playertoon.x1 = 100;
	_playertoon.x2 = 110;
	_playertoon.y1 = 180;
	_playertoon.y2 = 190;

	Menu();
	menuon = 1;
	u = 0;
	int tx = 65;
	int ty = 10;
	int c = 0;
	int y = 0;
	int g;
	go = 0;
	movemod = 1;

	while(1){

		if(s == 1){
			//Starts the game.
			printf("Creating character...\n");
			clear_screen();
			PlayerF(0);
			g = 1;
			s = 0;
			menuon = 0;
			write_string(tx,ty+2,"PRESS ENTER");
			write_string(tx,ty+3,"TO RETURN.");
		}else if(s == 2){
			//Ends the game loads the menu again.
			Menu();
			go = 0;
			g = 0;
			_playertoon.x1 = 100;
			_playertoon.x2 = 110;
			_playertoon.y1 = 180;
			_playertoon.y2 = 190;
			s = 0;
			menuon = 1;
		}

		//Makes the player character jump.
		if(u == 1 && go == 0){
			while(c != 5){
				if(get_Pixel(_playertoon.x1, _playertoon.y1 - 1) == 0x0000 && get_Pixel(_playertoon.x2, _playertoon.y1 - 1) == 0x0000){
					write_rect(_playertoon.x1, _playertoon.x2, _playertoon.y1, _playertoon.y2, VGAColour_Black);
					_playertoon.y1 -= movemod;
					_playertoon.y2 -= movemod;
					write_rect(_playertoon.x1, _playertoon.x2, _playertoon.y1, _playertoon.y2, VGAColour_White);
				}
				c++;
			}
				c = 0;
				y++;
				if(y == 8){
					y = 0;
					u = 0;
					OSTimeDlyHMSM(0, 0, 0, 500);
			}
		}

		//Gravity for the player character.
		if(u == 0 && g == 1){
			while(c != 3){
				if(get_Pixel(_playertoon.x1, _playertoon.y2 + 1) == 0x0000 && get_Pixel(_playertoon.x2, _playertoon.y2 + 1) == 0x0000){
					write_rect(_playertoon.x1, _playertoon.x2, _playertoon.y1, _playertoon.y2, VGAColour_Black);
					_playertoon.y1 += movemod;
					_playertoon.y2 += movemod;
					write_rect(_playertoon.x1, _playertoon.x2, _playertoon.y1, _playertoon.y2, VGAColour_White);
				}
				c++;
			}
			c = 0;
		}

		//If the player goes below the screen, stop the game and show gameover.
		if(_playertoon.y2 + 1 >= 240){
			write_string(tx,ty,"GAME OVER!");
			go = 1;
		}
		OSTimeDlyHMSM(0, 0, 0, 100);
	}
}

/*
 * This task reads the keyboard on the ps/2 port.
 * Author: Mike Griep
 */
void TaskStart(void* pdata)
{
	printf("TaskStart started.\n");
	char byte1 = 0;
	char byte2 = 0;
	char byte3 = 0;
	int c = 1;

  	volatile int * PS2_ptr = (int *) 0x10000100;

	int PS2_data, RVALID;

	while (1) {
		//read the ps/2 port.
		PS2_data = *(PS2_ptr);
				RVALID = PS2_data & 0x8000;
				if (RVALID)
				{
					byte1 = byte2;
					byte2 = byte3;
					byte3 = PS2_data & 0xFF;

					if((byte2 == (char) 0xE0) && (byte3 == (char) 0x74)){
						printf("->\n");
						if(menuon == 0){
							PlayerF(1);
						}
					}
					if((byte2 == (char) 0xE0) && (byte3 == (char) 0x6B)){
						printf("<-\n");
						if(menuon == 0){
							PlayerF(2);
						}
					}
					if((byte2 == (char) 0xF0) && (byte3 == (char) 0x29)){
						printf("( )\n");
						if(menuon == 0){
							u = 1;
						}
					}
					if((byte2 == (char) 0xF0) && (byte3 == (char) 0x75)){
						if(menuon == 0){
							u = 1;
						}
					}
					if((byte2 == (char) 0xF0) && (byte3 == (char) 0x5A)){
						printf("(\\n)\n");
						printf("toggling menu\n");
						if(c == 3){
							c = 1;
						}
						s = c;
						c++;

					}
				}
		OSTimeDlyHMSM(0, 0, 0, 50);
	}
}

int main(void)
{
  
	OSInit();

	OSTaskCreate(TaskGame, NULL, &G_stk[TASK_STACKSIZE-1], TASKRK_PRIORITY);
	OSTaskCreate(TaskStart, NULL, &RK_stk[TASK_STACKSIZE-1], TASKLV_PRIORITY);
	OSTaskCreate(TaskLevel, NULL, &LV_stk[TASK_STACKSIZE-1], TASKG_PRIORITY);

              
  OSStart();
  return 0;
}

/*
 * This function checks if its possible and then moves the player character left and right.
 * Author: Mike Griep
 */
void PlayerF(char k){
	int c = 0;
	int i;
	int yes = 0;
	write_rect(_playertoon.x1, _playertoon.x2, _playertoon.y1, _playertoon.y2, VGAColour_White);

	//Checks if there are colored pixels to the right and Moves the player to the right.
	if(k == 1 && go == 0){
		while(c != 7){
			for (i = _playertoon.y1; i <= _playertoon.y2; i++)
			{
				if (get_Pixel(_playertoon.x2 + 1, i) != 0x0000)
					yes = 1;
			}
			if (!yes)
			{
				write_rect(_playertoon.x1, _playertoon.x2, _playertoon.y1, _playertoon.y2, VGAColour_Black);
				_playertoon.x1 += movemod;
				_playertoon.x2 += movemod;
				write_rect(_playertoon.x1, _playertoon.x2, _playertoon.y1, _playertoon.y2, VGAColour_White);
			}
			c++;
		}
	//Checks if there are colored pixels to the left and Moves the player to the left.
	}else if(k == 2 && go == 0){
		while(c != 7){
			for (i = _playertoon.y1; i <= _playertoon.y2; i++)
			{
				if (get_Pixel(_playertoon.x1 - 1, i) != 0x0000)
					yes = 1;
			}

			if(!yes)
			{
				write_rect(_playertoon.x1, _playertoon.x2, _playertoon.y1, _playertoon.y2, VGAColour_Black);
				_playertoon.x1 -= movemod;
				_playertoon.x2 -= movemod;
				write_rect(_playertoon.x1, _playertoon.x2, _playertoon.y1, _playertoon.y2, VGAColour_White);
			}
			c++;
		}
	}
}

