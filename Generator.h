#ifndef __GENERATOR__
#define __GENERATOR__

#include <stdio.h>
#include <stdlib.h>

#define MAX_STORED_LINES	10
#define LINE_GAP			30
#define LVL_STEP_DST		3

/*	About the #defines
 * 	* MAX_STORED_LINES	->	Number of lines the application stores.
 * 	* LINE_GAP			->	Standard distance between each line
 * 	* LVL_STEP_DST		->	Distance that the lines move down each step.
 */

typedef struct{	//	Definition of a line that gets stored.
	int y;		//	current Y-coordinate
	int gap;	//	X-coordinate of the gap
				//	if 0, then base line
} lvlLine;

volatile lvlLine Lines[MAX_STORED_LINES];	//	Array van opgeslagen lijnen

int levelInit();
void levelStep();
lvlLine* lines_refresh(lvlLine *line, int i);
int previous(int i);

#endif
