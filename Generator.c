#include "Generator.h"

/*	levelInit
 * 	Takes: void
 * 	Returns: int
 *
 * 	Initialises the first set of lines
 * 	returned int is for safety checking
 */

int levelInit()
{

	int i = 0;
	lvlLine* base;
	srand(time(NULL));

	printf("Generator.c activated\n");
	base = (lvlLine*) malloc(sizeof(lvlLine));
	if (NULL == base)
	{
		printf("Level init failed\n");
		printf("Base line could not be allocated\n");
		return -1;
	}
	base->y = 202;
	base->gap = 0;
	printf("baseline defined\n");
	Lines[i] = *base;
	i++;

	while(i < MAX_STORED_LINES)
	{
		lvlLine* newLine;
		newLine = (lvlLine*) malloc(sizeof(lvlLine));
		newLine = lines_refresh(newLine, i);
		if(NULL == &newLine)
			return -1;
		Lines[i] = *newLine;
		printf("line %2d defined\n", i);
		i++;
	}
	return 0;
}

/*	levelStep
 * 	Takes: void
 * 	Returns: void
 *
 * 	Moves all the lines down by LVL_STEP_DST.
 */

void levelStep()
{
	//stuff
	int i;
	for(i = 0; i < MAX_STORED_LINES; i++)
	{
		Lines[i].y += LVL_STEP_DST;
		if (Lines[i].y > 239)
		{
			Lines[i] = *lines_refresh(&Lines[i], i);
			//printf("Line %2d is nu y = %3d, gap = %3d", i, Lines[i].y, Lines[i].gap);
		}
	}
}

/*	lines_refresh
 *  takes: lvlLine pointer, int
 *  returns: lvlLine pointer
 *
 *  Moves the y-coordinate of Lines[int] to the top and rerolls its gap
 *
 *  May be a little inefficient through pointers 'n stuff, but it works.
 */

lvlLine* lines_refresh(lvlLine *line, int i)
{
	line->y = Lines[previous(i)].y - LINE_GAP;
	line->gap = (rand() % 210) + 10;
	return line;
}
/*	previous
 * 	takes: int
 * 	returns: int
 *
 * 	Returns the previous in the array of stored lines.
 */

int previous(int i)
{
	i--;
	if (i < 0)
		return MAX_STORED_LINES - 1;
	return i;
}
